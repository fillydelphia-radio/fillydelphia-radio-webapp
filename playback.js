/*global soundManager*/

var radioPlayback;

function startPlayback() {
    radioPlayback = soundManager.createSound({
		id: 'radioaudio',
		url: 'http://fillyradio.com:8012/128k_mp3',
		autoLoad: false,
		autoPlay: false,
		volume: 50,
  	});
}

function startRadio() {
	jQuery(".playradio").hide();
	jQuery(".stopradio").show();
    startPlayback();
	soundManager.play('radioaudio');
}

function stopRadio() {
	jQuery(".stopradio").hide();
	jQuery(".playradio").show();
	soundManager.stop('radioaudio');
	soundManager.unload('radioaudio');
}
soundManager.setup({ url: '/sm2swf/', flashVersion: 9 });