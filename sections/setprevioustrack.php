<?php
/**
 * Created by PhpStorm.
 * User: westj
 * Date: 10/03/16w
 * Time: 12:51 PM
 */

$prevtracks = new PDO('mysql:host=127.0.0.1;dbname=prevtrack;charset=utf8mb4', 'prevtrack', 'M4ranatha');
$prevtracks->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$prevtracks->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$title = htmlspecialchars($_GET["title"]);
$artist = htmlspecialchars($_GET["artist"]);
$album = htmlspecialchars($_GET["album"]);
$year = htmlspecialchars($_GET["year"]);
$comment = htmlspecialchars($_GET["comment"]);
$track = htmlspecialchars($_GET["track"]);

try {
    $sql = $prevtracks->prepare("INSERT INTO prevtracktable (title, artist, album, year, comment, track) VALUES (:title, :artist, :album, :year, :comment, :track)");
    $sql->bindParam(':title', $title);
    $sql->bindParam(':artist', $artist);
    $sql->bindParam(':album', $album);
    $sql->bindParam(':year', $year);
    $sql->bindParam(':comment', $comment);
    $sql->bindParam(':track', $track);
    $sql->execute();
    $stmt = $prevtracks->query('DELETE from prevtracktable WHERE time < NOW() - INTERVAL 48 HOUR');
    $stmt->execute();
}
catch(PDOException $ex) {
    echo "ERROR: The Reporting Script has encountered an error - " . $ex; //user friendly message
}
