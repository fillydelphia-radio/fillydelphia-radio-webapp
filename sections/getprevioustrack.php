<?php
/**
 * Created by PhpStorm.
 * User: westj
 * Date: 10/03/16
 * Time: 12:51 PM
 */

header('Content-Type: application/json');

$getprevtracks = new PDO('mysql:host=127.0.0.1:3306;dbname=prevtrack;charset=utf8mb4', 'prevtrack', 'M4ranatha');
$getprevtracks->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$getprevtracks->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

try {
    #$stmt = $getprevtracks->query('DELETE from prevtracktable WHERE time < NOW() - INTERVAL 48 HOUR');
    #$clearold = $stmt->execute();
    $stmt = $getprevtracks->query('select * from prevtracktable ORDER BY id DESC LIMIT 10;');
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($data);
}
catch(PDOException $ex) {
    echo $ex; //user friendly message
}
