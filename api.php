<?php
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');

// Length of time to preserve local cache.
$cache_threshold = 5;

$local_cache = dirname(__FILE__).'/api.json';

$cache_time = time()-$cache_threshold;

if (file_exists($local_cache) && filemtime($local_cache) >= $cache_time)
{
 $file_raw = file_get_contents($local_cache);
 echo $file_raw;
}
else
{
 $api_url = 'http://fillyradio.com:8014/getmeta';

 $file_raw = @file_get_contents($api_url);

 if($file_raw === FALSE) { echo 'Fillydelphia Radio API Unavailable'; }
 
 if (!empty($file_raw))
 {
  // Write to local cache.
  file_put_contents($local_cache, $file_raw);

  echo $file_raw;
 }
 else
 {
  $file_raw = file_get_contents($local_cache);
  echo $file_raw;
 }
}
?>