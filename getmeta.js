var cache = new LastFMCache();

var lastfm = new LastFM({
    apiKey    : '7abaa5b78a65a61395f589e616c8f008',
    cache     : cache
});

var metaCoverCheck = null;
var coveronecheck = null;
var covertwocheck = null;
var coverthreecheck = null;

function getStats() {
    jQuery.ajax({
        url: 'https://fillydelphiaradio.net/api.php', dataType: 'json', success: function(stats) {
            var album = null;
            var largestCoverUrl = null;
            var metaArtist = stats.artist;
            var metaAlbum = stats.album;
            var metaCover = null;
            var metaCoverAlbumQuery = null;
            var metaPresenter = 'Delphia';
            var metaTrack = stats.title;

            
            if(metaAlbum == null) {
                jQuery('.big-album-pre').html('');
                metaAlbum = 'Could not get Album';
                metaCoverAlbumQuery = metaTrack;
                jQuery('.big-album').css("color", "#FF9494");
                
            } else {
                jQuery('.big-album-pre').html('From the album: ');
                jQuery('.big-album').css("color", "white");
                metaCoverAlbumQuery = metaAlbum;
            }
            
            if(metaTrack == null) {
                metaTrack = 'Unknown Song';
                jQuery('.big-track').css("color", "#FF9494");
                
            } else {
                jQuery('.big-track').css("color", "white");
            }
            
            if(metaArtist == null) {
                metaArtist = 'Unknown Artist';
                jQuery('.big-artist').css("color", "#FF9494");
                
            } else {
                jQuery('.big-artist').css("color", "white");
            }
            
            if(metaPresenter == null) {
                jQuery('.big-pre-presenter').html('');
                metaPresnter = 'Could not get Presenter Info';
                jQuery('.big-presenter').css("color", "#FF9494");
                
            } else {
                jQuery('.big-pre-presenter').html('Current DJ/Show: ');
                jQuery('.big-presenter').css("color", "white");
            }
            
            // Process Album
            if ((metaAlbum.length >= 1) && (metaAlbum.length < 20)) {
                jQuery('.big-album').css("font-size", "2vw");
                jQuery('.big-album').html(metaAlbum);
                jQuery('.big-album-pre').css("font-size", "2vw");
            }
            else if ((metaAlbum.length >= 20) && (metaAlbum.length < 40)) {
                jQuery('.big-album').css("font-size", "1.8vw");
                jQuery('.album').html(metaAlbum);
                jQuery('.big-album-pre').css("font-size", "1.8vw");
            }
            else if ((metaAlbum.length >= 40) && (metaAlbum.length < 100)) {
                jQuery('.big-album').css("font-size", "1vw");
                jQuery('.album').html(metaAlbum);
                jQuery('.big-album-pre').css("font-size", "1vw");
            }
            else {
                jQuery('.big-album').css("font-size", "0.8vw");
                jQuery('.album').html(metaAlbum);
                jQuery('.big-album-pre').css("font-size", "0.8vw");
            }
            
            //Process Artist
            if ((metaArtist.length >= 1) && (metaArtist.length < 19)) {
                jQuery('.big-artist').css("font-size", "3.5vw");
                jQuery('.big-artist').css("line-height", "3.2vw");
                jQuery('.artist').html(metaArtist);
            }
            else if ((metaArtist.length >= 19) && (metaArtist.length < 60)) {
                jQuery('.big-artist').css("font-size", "2.5vw");
                jQuery('.big-artist').css("line-height", "2.1vw");
                jQuery('.artist').html(metaArtist);
            }
            else if (metaArtist.length >= 60) {
                jQuery('.big-artist').css("font-size", "1.5vw");
                jQuery('.artist').html(metaArtist);
            }
            
            //Process Track
            if ((metaTrack.length >= 1) && (metaTrack.length < 19)) {
                jQuery('.big-track').css("font-size", "3.5vw");
                jQuery('.track').html(metaTrack);
            }
            else if ((metaTrack.length >= 19) && (metaTrack.length < 40)) {
                jQuery('.big-track').css("font-size", "2.5vw");
                
                jQuery('.track').html(metaTrack);
            }
            else {
                jQuery('.big-track').css("font-size", "1.5vw");
                jQuery('.track').html(metaTrack);
            }
            
            //Process Presenter
            if ((metaPresenter.length >= 1) && (metaPresenter.length < 19)) {
                jQuery('.big-presenter').css("font-size", "2.5vw");
                jQuery('.big-pre-presenter').css("font-size", "2.5vw");
                jQuery('.presenter').html(metaPresenter);
            }
            else if ((metaPresenter.length >= 19) && (metaPresenter.length < 40)) {
                jQuery('.big-presenter').css("font-size", "2vw");    
                jQuery('.big-pre-presenter').css("font-size", "2vw");
                jQuery('.presenter').html(metaPresenter);
            }
            else if (metaPresenter.length >= 40) {
                jQuery('.big-presenter').css("font-size", "1.5vw");
                jQuery('.big-pre-presenter').css("font-size", "1.5vw");
                jQuery('.presenter').html(metaPresenter);
            }

            lastfm.album.search({album: metaArtist + ' ' + metaCoverAlbumQuery}, {success: function(data) {
                album = data.results.albummatches.album[0];
                if (album) {
                    largestCoverUrl = album.image[album.image.length - 1]['#text'];
                }
                else {
                    metaCover = '/nocover.jpg';
                }

                if (largestCoverUrl) {
                    metaCover = largestCoverUrl;
                }
                else {
                    metaCover = '/nocover.jpg';
                }
                if (metaCover != metaCoverCheck) {
                    jQuery('.album-cover').fadeTo('slow', 0.3, function () {
                        jQuery(this).css('background-image', 'url("' + metaCover + '")');
                    }).fadeTo('slow', 1);
                    metaCoverCheck = metaCover;
                }

                console.log(largestCoverUrl);
            }, error: function(code, message){
                console.log(code, message);
            }});
            
            
        },
        error: function(stats) {
            jQuery('.artist').html('Error');
            jQuery('.track').html('Something Went Wrong!');

        }
    });
    jQuery.ajax({
        url: 'https://testing.fillydelphiaradio.net/sections/getprevioustrack.php', dataType: 'json', success: function(prev) {
            jQuery('#album-one-artist').html(prev[1].artist);
            jQuery('#album-one-title').html(prev[1].title);

            var album1query = prev[1].album;
            console.log('Result of Album from LastFM: ' + prev[1].album);
            if(prev[1].album == '') {
                album1query = prev[1].title;
                console.log('No album for prevtrack1! Using"' + album1query + '"');
            }
            console.log('Search Query prevtrack 1: ' + prev[1].artist + ' ' + album1query);


            lastfm.album.search({album: prev[1].artist + ' ' + album1query}, {success: function(data1) {
                album = data1.results.albummatches.album[0];
                if (album) {
                    var largestCoverUrl1 = album.image[album.image.length - 1]['#text'];
                }
                else {
                    var coverone = '/nocover.jpg';
                    console.log('Cover not found for prevtrack2!');
                }

                if (largestCoverUrl1) {
                    coverone = largestCoverUrl1;
                }
                else {
                    coverone = '/nocover.jpg';
                    console.log('Cover not found for prevtrack1!')
                }
                if (coverone != coveronecheck) {
                    jQuery('#album-one').fadeTo('slow', 0.3, function () {
                        jQuery(this).css('background-image', 'url("' + coverone + '")');
                    }).fadeTo('slow', 1);
                    coveronecheck = coverone;
                }
            }, error: function(code, message){
                console.log(code, message);
            }});

            jQuery('#album-two-artist').html(prev[2].artist);
            jQuery('#album-two-title').html(prev[2].title);

            var album2query = prev[2].album;
            console.log('Result of Album from LastFM: ' + prev[2].album);
            if(prev[2].album == '') {
                album2query = prev[2].title;
                console.log('No album for prevtrack2! Using"' + album2query + '"');
            }

            console.log('Search Query prevtrack 2: ' + prev[2].artist + ' ' + album2query);


            lastfm.album.search({album: prev[2].artist + ' ' + album2query}, {success: function(data2) {
                album = data2.results.albummatches.album[0];
                if (album) {
                    var largestCoverUrl2 = album.image[album.image.length - 1]['#text'];
                }
                else {
                    var covertwo = '/nocover.jpg';
                    console.log('Cover not found for prevtrack2!')
                }

                if (largestCoverUrl2) {
                    covertwo = largestCoverUrl2;
                }
                else {
                    covertwo = '/nocover.jpg';
                    console.log('Cover not found for prevtrack2!')
                }
                if (covertwo != covertwocheck) {
                    jQuery('#album-two').fadeTo('slow', 0.3, function () {
                        jQuery(this).css('background-image', 'url("' + covertwo + '")');
                    }).fadeTo('slow', 1);
                    covertwocheck = covertwo;
                }
            }, error: function(code, message){
                console.log(code, message);
            }});

            jQuery('#album-three-artist').html(prev[3].artist);
            jQuery('#album-three-title').html(prev[3].title);

            var album3query = prev[3].album;
            console.log('Result of Album from LastFM: ' + prev[3].album);
            if(prev[3].album == '') {
                album3query = prev[3].title;
                console.log('No album for prevtrack3! Using "' + prev[3].title + '"');
            }

            console.log('Search Query prevtrack 3: ' + prev[3].artist + ' ' + album3query);

            lastfm.album.search({album: prev[3].artist + ' ' + album3query}, {success: function(data3) {
                album = data3.results.albummatches.album[0];
                if (album) {
                    var largestCoverUrl3 = album.image[album.image.length - 1]['#text'];
                }
                else {
                    var coverthree = '/nocover.jpg';
                    console.log('Cover not found for prevtrack3!')
                }

                if (largestCoverUrl3) {
                    coverthree = largestCoverUrl3;
                }
                else {
                    coverthree = '/nocover.jpg';
                    console.log('Cover not found for prevtrack3!')
                }
                if (coverthree != coverthreecheck) {
                    jQuery('#album-three').fadeTo('slow', 0.3, function () {
                        jQuery(this).css('background-image', 'url("' + coverthree + '")');
                    }).fadeTo('slow', 1);
                    coverthreecheck = coverthree;
                }
            }, error: function(code, message){
                console.log(code, message);
            }});
        }});

}
jQuery( document ).ready(function() {
    getStats();
    setInterval(getStats, 10000);
});