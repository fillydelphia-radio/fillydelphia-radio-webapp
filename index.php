<?php

    $midsection = null;
    $botsection = null;

    $page = htmlspecialchars($_GET["page"]);

    if($page == null) {
        $midsection = "sections/home.html";
        $botsection = "sections/previousbar.html";
    }
    elseif ($page == "station") {
        $midsection = "sections/station.html";
        $botsection = "sections/playbar.html";
    }
    elseif ($page == "staff") {
        $midsection = "sections/staff.html";
        $botsection = "sections/playbar.html";
    }
    elseif ($page == "shows") {
        $midsection = "sections/shows.html";
        $botsection = "sections/playbar.html";
    }
    elseif ($page == "thanks") {
        $midsection = "sections/thanks.html";
        $botsection = "sections/playbar.html";
    }
    elseif ($page == "licenses") {
        $midsection = "sections/licenses.html";
        $botsection = "sections/playbar.html";
    }
    elseif ($page == "chat") {
        $midsection = "sections/chat.html";
        $botsection = "sections/playbar.html";
    }
    elseif ($page == "donate") {
        $midsection = "sections/donate.html";
        $botsection = "sections/playbar.html";
    }
    elseif ($page == "volunteer") {
        $midsection = "sections/volunteer.html";
        $botsection = "sections/playbar.html";
    }
    else {
        header("HTTP/1.0 404 Not Found");
        http_response_code(404);
        $midsection = "sections/404.html";
        $botsection = "sections/playbar.html";
}
?>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Fillydelphia Radio - Radio for Bronies</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="equal-height-columns.css">
    <link rel="stylesheet" href="styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.ajaxify/5.9.5/ajaxify.min.js" type="text/javascript"></script>
</head>
<body>
    <div class="container-fluid main-section" id="content-holder">
        <div class="background"></div>
        <div class="dark-background-panel"></div>
        <div class="row top-bar container-height-25 fixwidth">
            <nav class="col-lg-10 col-lg-offset-1">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="glyphicon glyphicon-menu-hamburger white"></span>
                    </button>
                    <a class="navbar-brand hidden-xs hidden-sm logo-lg" href="/"><img src="logo-small.png"></a>
                    <a class="navbar-brand show-xs show-sm hidden-lg hidden-md logo-xs" href="/"><img src="logo-xsmall.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/">Home</a></li>
                        <li>
                            <a href="#" class="dropdown-toggle no-ajaxy" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/station">The Station</a></li>
                                <li><a href="/staff">The Staff</a></li>
                                <li><a href="/shows">Shows</a></li>
                                <li><a href="/thanks">Thanks</a></li>
                                <li><a href="/licenses">Licenses</a></li>
                            </ul>
                        </li>
                        <li class="hidden-xs"><a href="/chat">Chat</a></li>
                        <li class="hidden-sm hidden-md hidden-lg"><a href="/chat">Chat</a></li>
                        <li><a href="/donate">Donate</a></li>
                        <li><a href="/volunteer">Volunteer</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Social<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Tumblr</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <?php readfile($midsection); ?>
        <div class="bottom-bar row-eq-height white">
            <?php readfile($botsection); ?>
        </div>
    </div>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,400italic,300italic,200italic' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="js/lastfm.api.md5.js"></script>
    <script type="text/javascript" src="js/lastfm.api.js"></script>
    <script type="text/javascript" src="js/lastfm.api.cache.js"></script>
    <script src="getmeta.js"></script>
    <script src="soundmanager2-jsmin.js"></script>
    <script src="playback.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            jQuery('#content-holder').ajaxify();
        });
        $(window).on('pronto.render', function(event, eventInfo){
            getStats();
            if(soundManager.getSoundById('radioaudio') != false) {
                jQuery(".playradio").hide();
                jQuery(".stopradio").show();
            };
        })
    </script> 
</body>
