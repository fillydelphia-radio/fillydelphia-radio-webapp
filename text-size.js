$(function resizeText(){
    
    //Define the elements
    var $album = $(".big-album");
    var $prealbum = $(".big-album-pre");
    var $artist = $(".big-artist");
    var $track = $(".big-track");
    var $presenter = $(".big-presenter");
    var $prepresenter = $(".big-pre-presenter");
    
    //Define the counters
    var $albumtext = $album.text().length;
    var $artisttext = $artist.text().length;
    var $tracktext = $track.text().length; 
    var $presentertext = $presenter.text().length; 
    
    // Process Album
    if (($albumtext >= 1) && ($albumtext < 40)) {
        $album.css("font-size", "2vw");
        $prealbum.css("font-size", "2vw");
    }
    else if (($albumtext >= 40) && ($albumtext < 60)) {
        $album.css("font-size", "1.8vw");
        $prealbum.css("font-size", "1.8vw");
    }
    else if (($albumtext >= 60) && ($albumtext < 100)) {
        $album.css("font-size", "1vw");
        $prealbum.css("font-size", "1vw");
    }
    else {
        $album.css("font-size", "0.8vw");
        $prealbum.css("font-size", "0.8vw");
    }
    
    //Process Artist
    if (($artisttext >= 1) && ($artisttext < 19)) {
        $artist.css("font-size", "3.5vw");
    }
    else if (($artisttext >= 19) && ($artisttext < 40)) {
        $artist.css("font-size", "2.5vw");        
    }
    else if ($artisttext >= 40) {
        $artist.css("font-size", "1.5vw");
    }
    
    //Process Track
    if (($tracktext >= 1) && ($tracktext < 19)) {
        $track.css("font-size", "3.5vw");
    }
    else if (($tracktext >= 19) && ($tracktext < 40)) {
        $track.css("font-size", "2.5vw");    
    }
    else if ($tracktext >= 40) {
        $track.css("font-size", "1.5vw");
    }
    
    //Process Presenter
    if (($presentertext >= 1) && ($presentertext < 19)) {
        $presenter.css("font-size", "2.5vw");
        $prepresenter.css("font-size", "2.5vw");
    }
    else if (($presentertext >= 19) && ($presentertext < 40)) {
        $presenter.css("font-size", "2vw");    
        $prepresenter.css("font-size", "2vw");    
    }
    else if ($presentertext >= 40) {
        $presenter.css("font-size", "1.5vw");
        $prepresenter.css("font-size", "1.5vw");
    }
})